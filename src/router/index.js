import Vue from 'vue'
import Router from 'vue-router'
import home from '@/pages/HomePage'
import login from '@/pages/LogIn'
import signup from '@/pages/SignUp'
import about from '@/pages/AboutApp'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/LogIn',
      name: 'login',
      component: login
    },
    {
      path: '/SignUp',
      name: 'signup',
      component: signup
    },
    {
      path: '/AboutApp',
      name: 'about',
      component: about
    }
  ]
})
